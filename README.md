## Installing server 
1. Install "XAMPP". Here is a download link "https://www.apachefriends.org/index.html".

2. Copy the folder "Website" in "kassasystem\Project\" and paste into "xampp\htdocs\".

3. Start "Xampp Control Panel" and start Apache. (If it fails see "Fix Apache startup error").
![example1](img1.PNG)

## Opening the website
1. Start your browser and type "http://localhost:your_port_number/The_Folder_In_Xampp\htdocs_/index.php".


## Fix Apache startup error
1. Click "Config" and then the option "Apache (httpd.conf)".
![example2](img2.PNG)

2. Search for the word "Listen 80" and replace it with an open port (eg. "8080").
![example3](img3.PNG)

## Opening the database
1. Start "Xampp Control Panel" and start Apache.
2. Start MySQL.

## Change database directory
> Stop MySQL if it is running

1. Go to your xampp installation directory.  
2. Open the folder mysql > bin  
3. Open the file my.ini  
4. Find the text "datadir" and change its directory value to C:\YOUR\PATH\TO\PROJECT\kassasystem\Project\data

## Where to manage databases
1. Start "Xampp Control Panel" and start Apache and then MySQL.
3. Click on Admin for MySQL.

## Install html5validator
You need to have pip installed and added to the path beforehand.

Open the command prompt and type the following command to control that you have pip installed and added to the path: 

    pip --version

If it says the command was not found, follow the instructions to how to install pip before continuing.

After that, write the following line in the command prompt.

    pip install html5validator==0.3.1
    
## Install pip
pip should already be installed on your system if you have python 3 or older installed. 

Open the command prompt and type the following command to control that you have python installed and added to the path: 

    python --version

If it says the command was not found, follow the instructions to how to install python before continuing.

After that, write the following line in the command prompt to control that you have pip installed and added to the path.

    pip --version

If it still says command not found, go to the following folder and check if you have the file pip.exe:

>C:\Users\Your-User-Name\AppData\Roaming\Python\Python37\Scripts

If you can't locate the above folder... ¯\\__(ツ)\__/¯ 

If you do have the pip.exe file, follow the instructions below about how to add pip to path.

If you do not have the pip.exe file in the above folder, go to the following website:


> https://pip.pypa.io/en/stable/installing/

and click the link to download get-pip.py.


After downloading get-pip.py, open the command prompt and navigate to the folder you downloaded the file to, then type the following command: 

    python get-pip.py --user

If the install was unsuccessful...¯\\__(ツ)\__/¯

If the install was successful, check the above folder again if the file pip.exe was added.

Now you have to add pip to the path.

### Add pip to path 

1. Navigate to the: control panel -> System and Security -> System

2. Click on "Change settings" and a system properties window should pop up. 

3. Click on the tab "Advanced" and then click on the button "Environment Variables..." near the bottom. 

4. Click on the text called "Path" at the top table of variables, then click the button "Edit..."

5. Press "New" and paste in the directory folder to the pip.exe file.

6. Press "Ok" for all the open windows and then restart the computer. Now pip is added to the path and you should be able to use the pip command in the command prompt.

## Install python

Go to the following folder and check if it exists. 

>C:\Program Files (x86)\Microsoft Visual Studio\Shared\Python37_64

If the above folder exists and you have the file python.exe in it, then python is already installed on your system. You only need to follow the instruction about how to add python to the path.

If you do not have the following folder, you will need to download and install python.

Go to the following website and download python.
>https://www.python.org/downloads/

Open the installation and make sure you have checked the checkbox called "Add python to PATH". Now you can skip the instruction about how to add python to path because the installation will do it for you. Complete the installation and you should now be able to use python AND pip at the command prompt.

### Add python to path

1. Navigate to the: control panel -> System and Security -> System

2. Click on "Change settings" and a system properties window should pop up. 

3. Click on the tab "Advanced" and then click on the button "Environment Variables..." near the bottom. 

4. Click on the text called "Path" at the top table of variables, then click the button "Edit..."

5. Press "New" and paste in the directory folder to the python.exe file.

6. Press "Ok" for all the open windows and then restart the computer. Now python is added to the path and you should be able to use the python command in the command prompt.