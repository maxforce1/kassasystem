<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" type="text/css" href="bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="style.css">
	<script src="script.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<title>Kassasystem</title>

	<?php

	$food = array(
		array("Capricciosa", "90&nbsp;kr", "pizza"),
		array("Calzone", "90&nbsp;kr", "pizza"),
		array("Margarita", "90&nbsp;kr", "pizza"),
		array("Hawaii", "90&nbsp;kr", "pizza"),
		array("Vesuvio", "90&nbsp;kr", "pizza"),
		array("Steak tartare", "185&nbsp;kr", "food"),
		array("Husmanstallrik", "125&nbsp;kr", "food"),
		array("Grillad tonfisksallad", "190&nbsp;kr", "seafood"),
		array("Fish and chips", "125&nbsp;kr", "seafood"),
		array("Räksallad", "120&nbsp;kr", "seafood"),
		array("Räkor 200g", "120&nbsp;kr", "seafood"),
		array("Rökta räkor 200g", "120&nbsp;kr", "seafood"),
		array("Havskräfta", "38&nbsp;kr", "seafood"),
		array("Halv krabba", "140&nbsp;kr", "seafood"),
		array("Halv hummer", "235&nbsp;kr", "seafood")
	);

	$drinks = array(
		array("Maison Sans Pareil Sauvignon Blanc", "135&nbsp;kr", "alc"),
		array("Maison Sans Pareil Sauvignon Blanc flaska", "495&nbsp;kr", "alc"),
		array("Paolo Leo Calaluna Fiano", "405&nbsp;kr", "alc"),
		array("Friedrich-Wilhelm-Gymnasium Riesling Trocken", "125&nbsp;kr", "alc"),
		array("Friedrich-Wilhelm-Gymnasium Riesling Trocken flaska", "455&nbsp;kr", "alc"),
		array("Jean-Claude Boisset, Pinot Noir", "135&nbsp;kr", "alc"),
		array("Jean-Claude Boisset, Pinot Noir flaska", "495&nbsp;kr", "alc"),
		array("I Castei Ripasso", "520&nbsp;kr", "alc"),
		array("Bistro Lager", "65&nbsp;kr", "alc"),
		array("Wisby Pils", "69&nbsp;kr", "alc"),
		array("Sleepy Bulldog Pale Ale", "75&nbsp;kr", "alc"),
		array("Grängesberg", "30&nbsp;kr", "alc"),
		array("Cruzcampo", "65&nbsp;kr", "alc"),
		array("Boulard Cidre De Normandie", "74&nbsp;kr", "alc"),
		array("Kaffe", "32&nbsp;kr", "coffee"),
		array("Espresso", "32&nbsp;kr", "coffee"),
		array("Dubbel espresso", " 36&nbsp;kr", "coffee"),
		array("Macchiato", "34&nbsp;kr", "coffee"),
		array("Dubbel macchiato", "42&nbsp;kr", "coffee"),
		array("Cappuccino", "44&nbsp;kr", "coffee"),
		array("Te", "34&nbsp;kr", "drinkmisc")

	);

	$desserts = array(
		array("Crème brûlée", "90&nbsp;kr", "dessert"),
		array("Hallonpavlova", "110&nbsp;kr", "dessert"),
		array("Passionsfruktssorbet", "50&nbsp;kr", "dessert")
	);

	$veg = array(
		array("Margarita", "90&nbsp;kr", "pizza")
	);

	$popular = array(
		$food[4],
		$food[7],
		$food[10],
		$drinks[3]
	);

	$tabs = array(
		array("Populära", "#004369", $popular),
		array("Maträtter", "#C40264", $food),
		array("Drycker", "#00ADA3", $drinks),
		array("Efterrätter", "#A9D639", $desserts),
		array("Vegetariskt", "#2C2C33", $veg)
	);





	function productGrid($products)
	{
		for ($x = 0; $x < count($products); $x++) {
			if ($x % 4 == 0 && $x != 0) {
				echo '</div>';
				echo '<div class="row mx-3" style="height: 200px;">';
			}
			echo '<div onClick="addOrderItem(\'' . $products[$x][0] . '\' ,\'' . $products[$x][1] . '\');" class="col border border-dark m-1 text-center px-0 productClass">';
			echo '<div class="categoryicon ' . $products[$x][2] . '"></div><p style="margin-top:40px; font-size: 20px;">' . $products[$x][0] . '</p>';
			echo '<p style="font-size: 20px;">' . $products[$x][1] . '</p>';
			echo '</div>';
		}
	}


	echo '<style>';
	for ($x = 0; $x < count($tabs); $x++) {
		echo '.tab' . $x . '{background-color: ' . $tabs[$x][1] . '} ';
		echo '#tab' . $x . ':target {background-color: ' . $tabs[$x][1] . '; color: white;}';
	}
	echo '</style>';

	?>
</head>

<body onload="location.assign('#tab0')">
	<script>
		<?php

		echo 'ids=[';
		for ($x = 0; $x < count($tabs); $x++) {
			echo '"tab' . $x . '", ';
		}
		echo '];';

		echo 'colors=[';
		for ($x = 0; $x < count($tabs); $x++) {
			echo '"' . $tabs[$x][1] . '", ';
		}
		echo '];';

		echo 'ids=[';
		for ($x = 0; $x < count($tabs); $x++) {
			echo '"tab' . $x . '", ';
		}
		echo '];';

		echo 'colors=[';
		for ($x = 0; $x < count($tabs); $x++) {
			echo '"' . $tabs[$x][1] . '", ';
		}
		echo '];';

		?>
	</script>

	<nav class="navclass border-bottom p-0 m-0">
		<div class=" p-0 m-0 row">
			<?php
			for ($x = 0; $x < count($tabs); $x++) {
				echo '<a id="tab' . $x . '" onclick="currentTab(\'tab' . $x . '\')" class="navbar-brand col py-5 mx-0 px-auto tab text-center active" href="#tab' . $x . '">';
				echo '<div class="tabcolorrow tab' . $x . '"></div>';
				echo $tabs[$x][0];
				echo '</a>';
			}
			?>

		</div>

		<div id="navbottom" style="background-color: #004369">

		</div>
	</nav>
	<div class="container">
		<?php

		for ($x = 0; $x <= 4; $x++) {
			echo '<div class="row border">';
			echo '</div>';
		}

		?>
	</div>



	<div class="container-fluid">
		<div class="row">
			<div class="col-9">

				<?php
				for ($x = 0; $x < count($tabs); $x++) {

					$style = "block";

					if ($x > 0) {

						$style = "none";
					}

					echo
						'
					<div id="grid' . $x . '" class="mt-5 grid-container" style="display: ' . $style . ';">
						<div class="row mx-3" style="height: 200px;">
					';

					productGrid($tabs[$x][2]);

					echo
						'</div>
					</div>
					';
				}
				?>

			</div>

			<div class="col-3">
				<div>
					<div class="cancelOrder" data-toggle="modal" data-target="#cancelModal">
						Nollställ beställning
					</div>
					<!-- The Modal -->
					<div class="modal fade" id="cancelModal">
						<div class="modal-dialog">
							<div class="modal-content">

								<!-- Modal body -->
								<div class="modal-body text-center">
									Är du säker på att du vill nollställa den nuvarande beställningen?
								</div>

								<!-- Modal footer -->
								<div class="modal-footer">
									<button onClick="cleanOrder()" type="button" class="btn btn-danger mr-auto" data-dismiss="modal">Nollställ</button>
									<button id="exitCleanBtn" type="button" class="btn ml-auto btn-light" data-dismiss="modal">Avbryt</button>
								</div>

							</div>
						</div>
					</div>
					<div class="orderList">
						<div class="orderTitle">
							Beställning
						</div>


						<div id="ORDERCONTAINER" class="orderContainer">
							<hr class="orderLine">

						</div>
						<div class="confirmOrder" data-toggle="modal" data-target="#confirmOrderModal">
							<div style="float: left;">Beställ</div>
							<span style="float: right;"><span id="totalSum">0</span>&nbsp;kr</span>
						</div>

												<!-- The Modal -->
												<div class="modal fade" id="confirmOrderModal">
							<div class="modal-dialog">
								<div class="modal-content">

									<!-- Modal body -->
									<div class="modal-body text-center">
										Är du säker på att du vill slutföra din beställning?
									</div>

									<!-- Modal footer -->
									<div class="modal-footer">
										<button id="confirmBtn" onClick="confirmOrder()" type="button" class="btn btn-success mr-auto" data-dismiss="modal">Beställ</button>
										<button id="exitConfirmBtn" type="button" class="btn ml-auto btn-light" data-dismiss="modal">Avbryt</button>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>

</html>