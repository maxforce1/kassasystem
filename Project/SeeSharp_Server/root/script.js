// list of ids and their colors
// example: "popular" has color "#004369"

var ids = []
var colors = []

// is callable from currenttab
function changeColor(color)
{
	// changes the color of current tab color
	document.getElementById("navbottom").style.backgroundColor=color;
}

function changeDisplay(id, ids)
{
	// hides all item grids
	for (i = 0; i < ids.length; i++) {
		grid = "grid" + i
		document.getElementById(grid).style.display = "none";
	}
	// displays the item grid of the current tab
	id = "grid" + id.slice(3, );
	document.getElementById(id).style.display = "block";
}

function currentTab(id)
{

	// goes through each id name
	for (arrayNumber in ids)
	{		
		
		if(document.getElementById(id).id == ids[arrayNumber])
		{	
			// calls the function changeColor that assign the color
			changeColor(colors[arrayNumber])
			changeDisplay(id, ids)

		}
	}
}

var orderElements = [];

// adds the clicked item to the order list and calculates the price
function addOrderItem(name, price)
{

	orderElements.push(["<div><div><img class='removeProduct' onClick=removeOrderItem() src=images/delete.png alt=Delete><p class=orderItem>"+ name + " " + price + "</p><hr class=orderLine></div>", parseInt(price)]);

	updateOrder();

	var sumElem = document.getElementById("totalSum");
	sumElem.innerHTML = parseInt(sumElem.innerHTML) + parseInt(price);
}

function removeOrderItem(index)
{

	var sumElem = document.getElementById("totalSum");

	sumElem.innerHTML = parseInt(sumElem.innerHTML) - orderElements[index][1];

	orderElements.splice(index, 1);

	updateOrder();

	
}

function updateOrder()
{
	var fullOrder = "<hr class=orderLine></div>";

	for (var i = 0; i < orderElements.length; i++)
	{
		var temp = orderElements[i][0];
		var pos = temp.indexOf(") src=");
		var temp2 = temp.substring(pos, temp.length);

		orderElements[i][0] = "<div><div><img class='removeProduct' onClick=removeOrderItem(" + i + temp2;

		fullOrder += orderElements[i][0];
	}

	console.log(fullOrder);
	console.log(orderElements);
	document.getElementById("ORDERCONTAINER").innerHTML = fullOrder;
}

// cleans the order list
function cleanOrder(){
	document.getElementById("ORDERCONTAINER").innerHTML = "<hr class=orderLine>";
	document.getElementById("totalSum").innerHTML = 0;
	orderElements = [];
}

// cleans the order list first and then...
function confirmOrder(){
	cleanOrder();
}