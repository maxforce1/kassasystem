﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using static System.Net.Mime.MediaTypeNames;

namespace WebServer
{
    public class WebServer
    {
        private readonly HttpListener _listener = new HttpListener();
        private readonly Func<HttpListenerRequest, string> _responderMethod;

        public WebServer(IReadOnlyCollection<string> prefixes, Func<HttpListenerRequest, string> method)
        {
            if (!HttpListener.IsSupported)
            {
                throw new NotSupportedException("Needs Windows XP SP2, Server 2003 or later.");
            }

            // URI prefixes are required eg: "http://localhost:1337/"
            if (prefixes == null || prefixes.Count == 0)
            {
                throw new ArgumentException("URI prefixes are required");
            }

            if (method == null)
            {
                throw new ArgumentException("responder method required");
            }

            foreach (var s in prefixes)
            {
                _listener.Prefixes.Add(s);
            }

            _responderMethod = method;
            _listener.Start();
        }

        public WebServer(Func<HttpListenerRequest, string> method, params string[] prefixes)
           : this(prefixes, method)
        {
        }

        public void Run()
        {
            ThreadPool.QueueUserWorkItem(o =>
            {
                Console.WriteLine("Webserver running...");
                try
                {
                    while (_listener.IsListening)
                    {
                        ThreadPool.QueueUserWorkItem(c =>
                        {
                            var ctx = c as HttpListenerContext;
                            try
                            {
                                if (ctx == null)
                                {
                                    return;
                                }

                                var rstr = _responderMethod(ctx.Request);
                                var buf = Encoding.UTF8.GetBytes(rstr);
                                ctx.Response.ContentLength64 = buf.Length;
                                ctx.Response.OutputStream.Write(buf, 0, buf.Length);
                            }
                            catch
                            {
                                // ignored
                            }
                            finally
                            {
                                // always close the stream
                                if (ctx != null)
                                {
                                    ctx.Response.OutputStream.Close();
                                }
                            }
                        }, _listener.GetContext());
                    }
                }
                catch (Exception ex)
                {
                    // ignored
                }
            });
        }

        public void Stop()
        {
            _listener.Stop();
            _listener.Close();
        }
    }

    internal class Program
    {
        public static string SendResponse(HttpListenerRequest request)
        {
            // Load content from index.html

            string currentDirPath = Directory.GetCurrentDirectory();

            Console.WriteLine(currentDirPath);

            // root files
            string path = Path.GetFullPath(Path.Combine(currentDirPath, @"..\..\root\"));

            Console.WriteLine("request: " + request.RawUrl);

            if (request.RawUrl == "/")
            {
                return PhpToHtml("index.php");
            }
            else
            {
                string rawUrl = request.RawUrl;

                try
                {

                    string revExtension = "";

                    for (int i = rawUrl.Length - 1; i > 0; i--)
                    {
                        if (rawUrl[i] == '.')
                        {
                            break;
                        }
                      else
                        {
                            revExtension += rawUrl[i];
                        }
                    }

                    char[] charArray = revExtension.ToCharArray();
                    Array.Reverse(charArray);

                    string extension = new string(charArray);

                    Console.WriteLine("extension: " + extension);

                    if (extension == "png")
                    {
                        // File.ReadAllBytes

                        byte[] file = File.ReadAllBytes(path + rawUrl);

                        /*
                        System.Drawing.Image img = System.Drawing.Image.FromFile(path + rawUrl);
                        Bitmap bitmap = new Bitmap("");

                        var stream = new MemoryStream();

                        bitmap.Save(stream, bitmap.RawFormat);

                        // bitmanp.Save(stream, img.RawFormat);
                        var bytes = stream.ToArray();

                        return Convert.ToBase64String(bytes);
                        */
                        // string file = File.ReadAllText(path + rawUrl, Encoding.Default);

                        // string result = Encoding.Default.GetString(file);

                        // return file;

                        return Convert.ToBase64String(file);
                    }
                    if (extension == "php")
                    {
                        return PhpToHtml(request.RawUrl);
                    }
                    else
                    {
                        return File.ReadAllText(path + request.RawUrl, Encoding.UTF8);
                    }
                }
                catch (Exception ex)
                {
                    // ignored

                    return "";
                }
            }
        }

        public static string PhpToHtml(string path)
        {
            try
            {
                using (Process myProcess = new Process())
                {
                    string currentDirPath = Directory.GetCurrentDirectory();

                    string exepath = Path.GetFullPath(Path.Combine(currentDirPath, @"..\..\php\php.exe"));

                    string filepath = Path.GetFullPath(Path.Combine(currentDirPath, @"..\..\root\" + path));

                    Console.WriteLine(filepath);

                    myProcess.StartInfo.RedirectStandardOutput = true;
                    myProcess.StartInfo.UseShellExecute = false;
                    myProcess.StartInfo.StandardOutputEncoding = Encoding.UTF8;
                    // You can start any process, HelloWorld is a do-nothing example.
                    myProcess.StartInfo.FileName = exepath;
                    myProcess.StartInfo.Arguments = Encoding.Default.GetString(Encoding.UTF8.GetBytes(filepath));
                    
                    myProcess.StartInfo.CreateNoWindow = true;
                    myProcess.Start();
                    Console.WriteLine("Process started");

                    var html = "";

                    while (!myProcess.StandardOutput.EndOfStream)
                    {
                        var line = myProcess.StandardOutput.ReadLine();

                        html += line;

                        // Console.WriteLine(line);
                    }

                    // return string
                    // Console.WriteLine(html);

                    return html;

                    // This code assumes the process you are starting will terminate itself. 
                    // Given that is is started without a window so you cannot terminate it 
                    // on the desktop, it must terminate itself or you can do it programmatically
                    // from this application using the Kill method.
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return "error";
            }
        }

        private static void Main(string[] args)
        {
            var ws = new WebServer(SendResponse, "http://localhost:1337/");
            ws.Run();
            Console.WriteLine("A simple webserver. Press a key to quit.");
            Console.ReadKey();
            ws.Stop();
        }
    }
}