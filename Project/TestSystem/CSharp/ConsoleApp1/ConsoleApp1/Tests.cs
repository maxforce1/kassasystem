﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Engine;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Test
{
    public class Tests
    {
        IWebDriver driver;
        string url = "http://localhost:8080/";

        string[] tabs =
        {
            "tab0",
            "tab1",
            "tab2",
            "tab3",
            "tab4",
        };

        string[] popularList =
        {
            "Vesuvio",
            "Grillad tonfisksallad",
            "Räkor 200g",
            "Friedrich-Wilhelm-Gymnasium Riesling Trocken",
        };

        string[] foodList =
        {
            "Capricciosa",
            "Calzone",
            "Margarita",
            "Hawaii",
            "Vesuvio",
            "Steak tartare",
            "Grillad tonfisksallad",
            "Fish and chips",
            "Husmanstallrik",
            "Räksallad",
            "Räkor 200g",
            "Rökta räkor 200g",
            "Havskräfta",
            "Halv krabba",
            "Halv hummer",
        };

        string[] drinkList =
        {
            "Maison Sans Pareil Sauvignon Blanc",
            "Paolo Leo Calaluna Fiano",
            "Friedrich-Wilhelm-Gymnasium Riesling Trocken",
            "Jean-Claude Boisset, Pinot Noir",
            "I Castei Ripasso",
            "Bistro Lager",
            "Wisby Pils",
            "Sleepy Bulldog Pale Ale",
            "Grängesberg",
            "Cruzcampo",
            "Kaffe",
            "Espresso",
            "Dubbel espresso",
            "Macchiato",
            "Dubbel macchiato",
            "Cappuccino",
            "Te",
            "Boulard Cidre De Normandie",
        };

        string[] dessertList =
        {
            "Crème brûlée",
            "Hallonpavlova",
            "Passionsfruktssorbet",
        };

        string[] vegList =
        {
            "Margarita",
        };

        //starts browser 
        [SetUp]
        public void startBrowser()
        {
            driver = new ChromeDriver();
            driver.Url = url;
        }

        //tests if the tabs can be clicked on
        [Test]
        public void testTab()
        {
            foreach (string start_tab in tabs)
            {
                driver.Url = url;

                foreach (string current_tab in tabs)
                {

                    IWebElement element = driver.FindElement(By.Id(current_tab));
                    element.Click();
                    Assert.AreEqual(driver.Url, url + "#" + current_tab, "Couldn't find "+ current_tab);
                    driver.Url = url + "#" + start_tab;
                    //😂
                }
            }

        }

        //test if popular contains the right items
        [Test]
        public void testPopular()
        {
            driver.FindElement(By.Id("tab0")).Click();
            foreach(string product in popularList)
            {
                bool existProduct = false;
                string element = driver.FindElement(By.Id("grid0")).Text;
                if (element.Contains(product))
                {
                    existProduct = true;
                }
                else
                {
                    existProduct = false;
                }
                Assert.IsTrue(existProduct, "The popular tab does not contain " + product);
            }
        }

        //test if food contains the right items
        [Test]
        public void testFood()
        {
            driver.FindElement(By.Id("tab1")).Click();
            foreach (string product in foodList)
            {
                bool existProduct = false;
                string element = driver.FindElement(By.Id("grid1")).Text;
                if (element.Contains(product))
                {
                    existProduct = true;
                }
                else
                {
                    existProduct = false;
                }
                Assert.IsTrue(existProduct, "The food tab does not contain " + product);
            }
        }

        //test if drinks contains the right items
        [Test]
        public void testDrink()
        {
            driver.FindElement(By.Id("tab2")).Click();
            foreach (string product in drinkList)
            {
                bool existProduct = false;
                string element = driver.FindElement(By.Id("grid2")).Text;
                if (element.Contains(product))
                {
                    existProduct = true;
                }
                else
                {
                    existProduct = false;
                }
                Assert.IsTrue(existProduct, "The drink tab does not contain " + product);
            }
        }

        //test if desserts contains the right items
        [Test]
        public void testDessert()
        {
            driver.FindElement(By.Id("tab3")).Click();
            foreach (string product in dessertList)
            {
                bool existProduct = false;
                string element = driver.FindElement(By.Id("grid3")).Text;
                if (element.Contains(product))
                {
                    existProduct = true;
                }
                else
                {
                    existProduct = false;
                }
                Assert.IsTrue(existProduct, "The dessert tab does not contain " + product);
            }
        }

        //test if veg contains the right items
        [Test]
        public void testVeg()
        {
            driver.FindElement(By.Id("tab4")).Click();
            foreach (string product in vegList)
            {
                bool existProduct = false;
                string element = driver.FindElement(By.Id("grid4")).Text;
                if (element.Contains(product))
                {
                    existProduct = true;
                }
                else
                {
                    existProduct = false;
                }
                Assert.IsTrue(existProduct, "The veg tab does not contain " + product);
            }
        }

        //Check if the items in testProducts where successfully added to the orderlist 
        [Test]
        public void testAddProduct()
        {
            driver.FindElement(By.Id("tab1")).Click();
            string[] testProducts = { "Calzone", "Hawaii", "Fish and chips"};
            IList<IWebElement> products = driver.FindElements(By.ClassName("productClass"));
            foreach(IWebElement product in products)
            {
                string productStr = product.Text;
                foreach (string testProduct in testProducts)
                {
                    if (productStr.Contains(testProduct))
                    {
                        product.Click();
                        string orderStr = driver.FindElement(By.ClassName("orderContainer")).Text;

                        bool existProduct = false;
                        if (orderStr.Contains(testProduct))
                        {
                            existProduct = true;
                            
                        }
                        else
                        {
                            existProduct = false;
                            
                        }
                        Assert.IsTrue(existProduct, testProduct + " not added to list");
                    }

                }
                
            }
            
        }

        //Checks that the total price is correct
        [Test]
        public void testCorrectPrice()
        {
            driver.FindElement(By.Id("tab1")).Click();
            string[] testProducts = { "Calzone", "Hawaii", "Fish and chips" };
            IList<IWebElement> products = driver.FindElements(By.ClassName("productClass"));
            foreach (IWebElement product in products)
            {
                string productStr = product.Text;
                foreach (string testProduct in testProducts)
                {
                    if (productStr.Contains(testProduct))
                    {
                        product.Click();
                    }
                }
            }
            string price = driver.FindElement(By.Id("totalSum")).Text;
            Assert.That(price.Contains("305"), "price didnt match");
        }

        //Clears order list and checks if order list is clear
        [Test]
        public void testClearProductList()
        {
            driver.FindElement(By.Id("tab1")).Click();
            string[] testProducts = { "Calzone", "Hawaii", "Fish and chips" };
            IList<IWebElement> products = driver.FindElements(By.ClassName("productClass"));
            foreach (IWebElement product in products)
            {
                string productStr = product.Text;
                foreach (string testProduct in testProducts)
                {
                    if (productStr.Contains(testProduct))
                    {
                        product.Click();
                    }
                }
            }
            driver.FindElement(By.ClassName("cancelOrder")).Click();
            System.Threading.Thread.Sleep(500);
            driver.FindElement(By.ClassName("btn-danger")).Click();
            IWebElement orderContainer = driver.FindElement(By.ClassName("orderContainer"));
            Assert.AreEqual("", orderContainer.Text);
        }

        //Confirms order and checks if order list is clear
        [Test]
        public void testConfirmOrder()
        {
            driver.FindElement(By.Id("tab1")).Click();
            string[] testProducts = { "Calzone", "Hawaii", "Fish and chips" };
            IList<IWebElement> products = driver.FindElements(By.ClassName("productClass"));
            foreach (IWebElement product in products)
            {
                string productStr = product.Text;
                foreach (string testProduct in testProducts)
                {
                    if (productStr.Contains(testProduct))
                    {
                        product.Click();
                    }
                }
            }
            driver.FindElement(By.ClassName("confirmOrder")).Click();
            System.Threading.Thread.Sleep(500);
            driver.FindElement(By.ClassName("btn-success")).Click();
            IWebElement orderContainer = driver.FindElement(By.ClassName("orderContainer"));
            Assert.AreEqual("", orderContainer.Text);
        }

        //Test remove single order item
        [Test]
        public void testRemoveItem()
        {
            driver.FindElement(By.Id("tab1")).Click();
            string[] testProducts = { "Calzone", "Hawaii", "Fish and chips" };
            IList<IWebElement> products = driver.FindElements(By.ClassName("productClass"));
            foreach (IWebElement product in products)
            {
                string productStr = product.Text;
                foreach (string testProduct in testProducts)
                {
                    if (productStr.Contains(testProduct))
                    {
                        product.Click();
                    }
                }
            }
            IList<IWebElement> orderContainer = driver.FindElements(By.ClassName("orderContainer"));
            foreach (IWebElement product in orderContainer)
            {
                string productStr = product.Text;
                foreach (string testProduct in testProducts)
                {
                    if (productStr.Contains(testProduct))
                    {
                        product.FindElement(By.ClassName("removeProduct")).Click();
                        System.Threading.Thread.Sleep(1000);
                    }
                }
            }
            IWebElement orderList = driver.FindElement(By.ClassName("orderContainer"));
            Assert.AreEqual("", orderList.Text);
        }

        [Test]


        [TearDown]
        public void closeBrowser()
        {
            driver.Close();
        }
    }
}