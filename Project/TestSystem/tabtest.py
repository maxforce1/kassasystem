from selenium import webdriver
from selenium.webdriver.firefox.options import Options
import unittest
import os
import io
import time

relativePath = os.path.dirname(__file__)

# PascalCase for classes
# camelCase for functions
# snake_case for variables

pathScreenshots = 'screenshots/'
url = 'http://localhost:8080/'

resolutions = [[800, 600], [768, 1024], [720, 1280], [1440, 2560], [1080, 1920], [750, 1334]]

navbarTabList = ["tab0", "tab1", "tab2", "tab3", "tab4"]

popularList = ["Vesuvio", "Grillad tonfisksallad", "Räkor 200g", "Friedrich-Wilhelm-Gymnasium Riesling Trocken"]
foodList = ["Capricciosa", "Calzone", "Margarita", "Hawaii", "Vesuvio", "Steak tartare", "Grillad tonfisksallad",
            "Fish and chips", "Husmanstallrik", "Räksallad", "Räkor 200g", "Rökta räkor 200g", "Havskräfta",
            "Halv krabba", "Halv hummer"]
drinkList = ["Maison Sans Pareil Sauvignon Blanc", "Paolo Leo Calaluna Fiano",
             "Friedrich-Wilhelm-Gymnasium Riesling Trocken", "Jean-Claude Boisset, Pinot Noir", "I Castei Ripasso",
             "Bistro Lager", "Wisby Pils", "Sleepy Bulldog Pale Ale", "Grängesberg", "Cruzcampo", "Kaffe", "Espresso",
             "Dubbel espresso", "Macchiato", "Dubbel macchiato", "Cappuccino", "Te", "Boulard Cidre De Normandie"]
dessertList = ["Crème brûlée", "Hallonpavlova", "Passionsfruktssorbet"]
vegList = ["Margarita"]

# define options for chrome
optionsChrome = webdriver.ChromeOptions()
# optionsChrome.add_argument("headless") # pass headless argument to the options (no ui)

default_driver = webdriver.Chrome(options=optionsChrome)


class Tests(unittest.TestCase):

    def url_check(self, url, msg):
        self.assertEqual(self.browser.current_url, url, msg)

    @classmethod
    def setUpClass(cls):
        # Runs resolution test on default_driver
        cls.browser = default_driver
        cls.browser.get(url)

    # Tests different resolutions and saves screenshots in the project folder
    # Also tests if the page is centered by saving screenshots in the project folder
    def testScreenshots(self):
        # for driver in list_drivers:
        # open with this browser
        self.browser = default_driver

        for current_tab in navbarTabList:
            self.browser.find_element_by_id(current_tab).click()
            for res in resolutions:
                self.browser.set_window_size(res[0], res[1])
                self.browser.get_screenshot_as_file(
                    pathScreenshots + current_tab + "Chrome" + str(res[0]) + "x" + str(res[1]) + '.png')

    def testTab(self):

        # goes through all tabs and take screenshots in different resolutions
        for start_tab in navbarTabList:
            self.browser.get(url)

            for current_tab in navbarTabList:
                try:
                    self.browser.find_element_by_id(current_tab).click()
                    print("found " + current_tab)
                except:
                    print("Couldn't find " + current_tab)

                self.url_check(url + "#" + current_tab, "Current url is not " + current_tab)
                self.browser.get(url + "#" + start_tab)
                print("back to " + start_tab)

    def testPopular(self):

        # clicks on the popular tab and checks if the popular item in popularList is present on the website
        self.browser.find_element_by_id("tab0").click()
        for product in popularList:
            with self.subTest(product=product):
                self.element = self.browser.find_element_by_id("grid0")
                self.assertIn(product, self.element.text)

    def testFood(self):

        # clicks on the food tab and checks if the food in foodList is present on the website
        self.browser.find_element_by_id("tab1").click()
        for product in foodList:
            with self.subTest(product=product):
                self.element = self.browser.find_element_by_id("grid1")
                self.assertIn(product, self.element.text)

    def testDrink(self):

        # clicks on the drink tab and checks if the drinks in drinkList is present on the website
        self.browser.find_element_by_id("tab2").click()
        for product in drinkList:
            with self.subTest(product=product):
                self.element = self.browser.find_element_by_id("grid2")
                self.assertIn(product, self.element.text)

    def testDessert(self):

        # clicks on the dessert tab and checks if the dessert in dessertList is present on the website
        self.browser.find_element_by_id("tab3").click()
        for product in dessertList:
            with self.subTest(product=product):
                self.element = self.browser.find_element_by_id("grid3")
                self.assertIn(product, self.element.text)

    def testVeg(self):

        # clicks on the veg tab and checks if the veg item in vegList is present on the website
        self.browser.find_element_by_id("tab4").click()
        for product in vegList:
            with self.subTest(product=product):
                self.element = self.browser.find_element_by_id("grid4")
                self.assertIn(product, self.element.text)

    def testAddProduct(self):
        # clicks on the food tab and clicks on all testProducts. Then checks if correct text is in orderContainer.
        self.browser.find_element_by_id("tab1").click()
        testProducts = ["Calzone", "Hawaii", "Fish and chips"]
        products = self.browser.find_elements_by_class_name("productClass")
        for product in products:
            for testProduct in testProducts:
                if testProduct in product.text:
                    product.click()
                    orderContainer = self.browser.find_element_by_class_name("orderContainer")

                    productText = product.text
                    productText = str(productText).replace("\n", " ")

                    self.assertIn(productText, orderContainer.text)

    def testCorrectPrice(self):
        self.browser.find_element_by_id("tab1").click()
        testProducts = ["Calzone", "Hawaii", "Fish and chips"]
        products = self.browser.find_elements_by_class_name("productClass")
        for product in products:
            for testProduct in testProducts:
                if testProduct in product.text:
                    product.click()

        price = self.browser.find_element_by_id("totalSum").textssssss
        self.assertIn("305", price)

    @classmethod
    # Closes the window after all the tests are done
    def tearDownClass(self):
        self.browser.quit()


class TestValidateWebsiteOffline(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        # Runs resolution test on default_driver
        cls.browser = default_driver
        cls.browser.get(url)

        # opens file or will create the file if it does not exist
        with io.open("htmlValidationFile.html", "w", encoding="utf-8") as f:
            # overwrites the file content with the page source
            f.write("<!DOCTYPE html>" + cls.browser.page_source)
            f.close()

    def testValidatorOffline(self):
        cmd = "html5validator -root htmlValidationFile.html"
        returned_value = os.system(cmd)  # returns the exit code in unix

        print('returned value:', returned_value)
        assert returned_value == 0


class TestClearOrder(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        # Runs resolution test on default_driver
        cls.browser = default_driver
        cls.browser.get(url)

    def testClearProductList(self):
        # clicks on the food tab and clicks on all testProducts. Then checks if correct text is in orderContainer.
        # then clears the list of added products and checks if all products were removed.
        self.browser.find_element_by_id("tab1").click()
        testProducts = ["Calzone", "Hawaii", "Fish and chips"]
        products = self.browser.find_elements_by_class_name("productClass")
        for product in products:
            for testProduct in testProducts:
                if testProduct in product.text:
                    product.click()
                    orderContainer = self.browser.find_element_by_class_name("orderContainer")

                    productText = product.text
                    productText = str(productText).replace("\n", " ")

                    self.assertIn(productText, orderContainer.text)

        # open modal (popup message)
        self.browser.find_element_by_class_name("cancelOrder").click()
        time.sleep(0.5)

        # clicks on the "Avbryt" button
        self.browser.find_element_by_id("exitCleanBtn").click()
        time.sleep(0.5)

        # open modal
        self.browser.find_element_by_class_name("cancelOrder").click()
        time.sleep(0.5)

        # clear the order
        self.browser.find_element_by_class_name("btn-danger").click()

        orderContainer = self.browser.find_element_by_id("ORDERCONTAINER")

        self.assertEqual("", orderContainer.text)


class TestConfirmOrder(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        # Runs resolution test on default_driver
        cls.browser = default_driver
        cls.browser.get(url)

    def testConfirmOrder(self):
        # clicks on the food tab and clicks on all testProducts. Then checks if correct text is in orderContainer.
        # then clears the list of added products and checks if all products were removed.
        self.browser.find_element_by_id("tab1").click()
        testProducts = ["Calzone", "Hawaii", "Fish and chips"]
        products = self.browser.find_elements_by_class_name("productClass")
        for product in products:
            for testProduct in testProducts:
                if testProduct in product.text:
                    product.click()
                    orderContainer = self.browser.find_element_by_class_name("orderContainer")

                    productText = product.text
                    productText = str(productText).replace("\n", " ")

                    self.assertIn(productText, orderContainer.text)

        # open modal (popup message)
        self.browser.find_element_by_class_name("confirmOrder").click()
        time.sleep(0.5)

        # clicks on the "Avbryt" button
        self.browser.find_element_by_id("exitConfirmBtn").click()
        time.sleep(0.5)

        # open modal
        self.browser.find_element_by_class_name("confirmOrder").click()
        time.sleep(0.5)

        # confirms the order
        self.browser.find_element_by_class_name("btn-success").click()
        time.sleep(0.5)

        orderContainer = self.browser.find_element_by_id("ORDERCONTAINER")

        self.assertEqual("", orderContainer.text)

class TestRemoveItem(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        # Runs resolution test on default_driver
        cls.browser = default_driver
        cls.browser.get(url)

    def testRemoveItem(self):
        # clicks on the food tab and clicks on all testProducts. Then checks if correct text is in orderContainer.
        self.browser.find_element_by_id("tab1").click()
        testProducts = ["Calzone", "Hawaii", "Fish and chips"]
        products = self.browser.find_elements_by_class_name("productClass")
        for product in products:
            for testProduct in testProducts:
                if testProduct in product.text:
                    product.click()
                    orderContainer = self.browser.find_element_by_class_name("orderContainer")
                    productText = product.text
                    productText = str(productText).replace("\n", " ")

                    self.assertIn(productText, orderContainer.text)

        orderContainer = self.browser.find_elements_by_class_name("orderContainer")

        for product in orderContainer:
            for testProduct in testProducts:
                if testProduct in product.text:
                    product.find_element_by_class_name("removeProduct").click()
                    time.sleep(1)
                    self.assertNotIn(testProduct, orderContainer)


if __name__ == '__main__':
    unittest.main()
