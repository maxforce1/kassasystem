﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace ConsoleApp3
{
    class Program
    {
        static void Main()
        {
            // port
            int port = 1234;

            // IpAddress  
            IPAddress ipaddress = IPAddress.Parse(GetLocalIPAddress());

            // root directory path
            string currentDirectory = Directory.GetCurrentDirectory();
            string root = Path.GetFullPath(Path.Combine(currentDirectory, @"..\..\..\new_server"));

            WebServer server = new WebServer();

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"Starting server at {ipaddress} : {port}");
            server.Start(ipaddress, port, root);
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("No network adapters with an IPv4 address in the system!");
        }
    }
}
